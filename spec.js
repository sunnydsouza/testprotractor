describe("main function to do stuff",function(){
	var firstNumber=element(by.model("first"));
	var secondNumber=element(by.model("second"));
	var selectDropdownButton=element(by.model("operator"));
	var submitButton=element(by.id("gobutton"));
	var endResult=element(by.className("ng-binding"));
	
	beforeEach(function(){
		browser.get("http://juliemr.github.io/protractor-demo/");
	});
	
	it("should be able to add two numbers",function(){
		firstNumber.sendKeys(1);
		secondNumber.sendKeys(2);
		submitButton.click();
		expect(endResult.getText()).toEqual("3");
	});
	
	it("should be able to subtract two numbers",function(){
		firstNumber.sendKeys(6);
		secondNumber.sendKeys(4);
		selectDropdownButton
			.element(by.cssContainingText('option', 'SUBTRACTION')).click;
		submitButton.click();
		expect(endResult.getText()).toEqual("2");
	});
})
